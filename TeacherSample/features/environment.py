#
#   Copyright (c) 2019 Joy Diamond.  All rights reserved.
#
from    TestingFramework    import  teardown


def after_scenario(context, scenario):
    teardown()
