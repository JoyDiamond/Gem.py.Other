#
#   Copyright (c) 2019 Joy Diamond.  All rights reserved.
#
import  Crystal


Z.copyright
Z.package.Crystal
Z.line(Crystal.vision)
